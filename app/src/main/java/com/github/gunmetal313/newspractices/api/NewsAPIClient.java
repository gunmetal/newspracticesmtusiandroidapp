package com.github.gunmetal313.newspractices.api;

import com.github.gunmetal313.newspractices.News;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface NewsAPIClient {
    //Получить список новостей
    @GET("news")
    Observable<List<News>> getNewsList(
    );

    //Создать новость
    @POST("news")
    Observable<ResponseBody> createNews(@Body News news
    );

    //Удалить новость по id
    @DELETE("news/{news_id}")
    Observable<ResponseBody> deleteNews(@Path("news_id") String newsId
    );

    //Новость по id
    @GET("news/{news_id}")
    Observable<News> getNews(@Path("news_id") String newsId
    );
}
