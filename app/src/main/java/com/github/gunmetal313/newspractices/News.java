package com.github.gunmetal313.newspractices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("main_text")
    private String main_text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMain_text() {
        return main_text;
    }

    public void setMain_text(String main_text) {
        this.main_text = main_text;
    }

    public News(String title, String main_text) {
        this.title = title;
        this.main_text = main_text;
    }
}
