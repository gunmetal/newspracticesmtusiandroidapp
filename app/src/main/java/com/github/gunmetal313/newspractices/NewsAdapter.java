package com.github.gunmetal313.newspractices;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    private List<News> newsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, short_description;
        public RelativeLayout viewForeground;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.title);
            short_description = (TextView) view.findViewById(R.id.short_description);
            viewForeground = (RelativeLayout) view.findViewById(R.id.view_foreground);
        }
    }

    public NewsAdapter(List<News> newsList) {
        this.newsList = newsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    public void removeItem(int position) {
        newsList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        News news = newsList.get(position);
        holder.name.setText(news.getTitle());
        holder.short_description.setText(news.getMain_text());
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}
