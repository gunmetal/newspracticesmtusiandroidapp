package com.github.gunmetal313.newspractices;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.github.gunmetal313.newspractices.api.APIClientUtils;
import com.github.gunmetal313.newspractices.api.NewsAPIClient;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class NewsAddActivity extends AppCompatActivity {

    TextInputEditText new_add_title_text;
    TextInputEditText new_add_content;
    private TextInputLayout textInputLayout;
    private EditText editText;

    private TextInputLayout textInputLayoutContent;
    private EditText editContent;
    private CompositeDisposable compositeDisposable;
    private NewsAPIClient client = APIClientUtils.getRetrofit();
    private FloatingActionButton fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        new_add_title_text = (TextInputEditText) findViewById(R.id.new_add_title_text);
        new_add_content = (TextInputEditText) findViewById(R.id.new_add_content);
        setSupportActionBar(toolbar);
        compositeDisposable = new CompositeDisposable();

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(view -> {
            compositeDisposable.add(client.createNews(new News(new_add_title_text.getText().toString(),new_add_content.getText().toString()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.single())
                    .subscribe(this::handleResponse, this::handleError));
        });
    }

    private void handleResponse(ResponseBody body) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void handleError(Throwable error) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, error.getLocalizedMessage(), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
