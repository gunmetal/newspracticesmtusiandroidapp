package com.github.gunmetal313.newspractices;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.gunmetal313.newspractices.api.APIClientUtils;
import com.github.gunmetal313.newspractices.api.NewsAPIClient;

import java.util.ArrayList;
import java.util.List;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    private static final String TAG = MainActivity.class.getName();
    ArrayList<News> newsList = new ArrayList<News>();
    NewsAPIClient client = APIClientUtils.getRetrofit();
    private NewsAdapter nAdapter;
    private RecyclerView recyclerView;
    private CompositeDisposable compositeDisposable;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        compositeDisposable = new CompositeDisposable();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        nAdapter = new NewsAdapter(newsList);
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(nAdapter);
        initSwipe();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.bringToFront();
        fab.setOnClickListener(view -> {
            Log.v(TAG, "OnClickListener");
            Intent intent = new Intent(this, NewsAddActivity.class);
            startActivity(intent);
        });

        loadNews();
    }

    private void initSwipe() {

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


        /*ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    private void loadNews(){

        compositeDisposable.add(client.getNewsList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));
    }

    private void handleResponse(List<News> news) {

        newsList = new ArrayList<News>(news);
        nAdapter = new NewsAdapter(newsList);
        recyclerView.setAdapter(nAdapter);
        recyclerView.invalidate();
    }

    private void handleError(Throwable error) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, error.getLocalizedMessage(), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        loadNews();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NewsAdapter.MyViewHolder) {
            String title = newsList.get(viewHolder.getAdapterPosition()).getTitle();
            nAdapter.removeItem(viewHolder.getAdapterPosition());

            View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, title + "был удалён", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
    }
}
